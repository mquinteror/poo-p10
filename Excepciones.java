public class Excepciones{
   public static void main(String args[]){
	 //bloque try catch padre
     try{
        System.out.println("Dentro del bloque 1");
        int b =45/0;
        System.out.println(b);

        System.out.println("Dentro del bloque 2");
        int c =45/0;

        System.out.println("Otra linea de codigo");
    }
    catch(ArithmeticException e3){
    	 System.out.println("Arithmetic Exception");
         System.out.println("Dentro del bloque padre");
    }
    catch(ArrayIndexOutOfBoundsException e4){
    	System.out.println("ArrayIndexOutOfBoundsException");
         System.out.println("Dentro del bloque padre");
    }
    catch(Exception e5){
    	System.out.println("Exception");
        System.out.println("Dentro del bloque padre");
    }
    System.out.println("Siguiente linea de codigo..");
  }
}
