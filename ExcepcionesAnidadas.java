public class ExcepcionesAnidadas{
   public static void main(String args[]){
	 //bloque try catch padre
     System.out.println("A) Iniciando bloque padre");
     try{
         //bloque try catch hijo 1
         try{
            System.out.println("B) Dentro del bloque 1");
            int b =Integer.parseInt(" 6  ");
            System.out.println("C)"+b);
         }
         catch(ArithmeticException e1){
            System.out.println("D) Exception: bloque 1");
         }
         //bloque try catch hijo 2 
         try{
            System.out.println("E) Dentro del bloque 2");
            int b =45/0;
            System.out.println("F)"+b);
         }
         catch(ArrayIndexOutOfBoundsException e2){
            System.out.println("G) Exception: bloque 2");
         }
        System.out.println("H) Otra linea de codigo");
    }
    catch(ArrayIndexOutOfBoundsException e3){
    	 System.out.println("I) ArrayIndexOutOfBoundsException");
         System.out.println("J) Dentro del bloque padre");
    }
    catch(NumberFormatException e4){
    	System.out.println("K) NumberFormatException");
         System.out.println("L) Dentro del bloque padre");
    }
    catch(Exception e5){
    	System.out.println("M) Exception");
        System.out.println("N) Dentro del bloque padre");
    }finally{
    	System.out.println("O) Bloque final");
    }
    System.out.println("P) Siguiente linea de codigo..");
  }
}

