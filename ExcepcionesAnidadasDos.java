public class ExcepcionesAnidadasDos{
    public static void main(String args[]){
        System.out.println("A) Iniciando bloque padre");
        //bloque try catch padre
        try{
            //bloque try catch 1
            try{
            //bloque try catch 2 
                try{
                    System.out.println("B) Dentro del bloque 2" );
                    int arreglo[] ={1, 2, 3, 4};
                    System.out.println("C)"+arreglo[4]);
                }catch(ArithmeticException e2){
                    System.out.println("D) Exception: bloque 2");
                }
            }catch(ArithmeticException e1){
                    System.out.println("E) Exception: bloque 1");
            }        
        }
        catch(ArithmeticException ep1){
            System.out.println("F) Arithmetic Exception");
        }
        catch(ArrayIndexOutOfBoundsException e4){
            System.out.println("G) ArrayIndexOutOfBoundsException");
        }
        catch(Exception e5){
            System.out.println("H) Exception");
        }

        System.out.println("I) Siguiente linea de codigo..");
    }
}

